//THIS SCRIPT CONTROLS FUNCTIONALITY

$(document).ready(function() {
  $('#search-icon').click(function(){
    $('.searchform').fadeToggle();
  });
  $('#toggle-todo').click(function(){
    $('.to-do-list-container').toggleClass('td-open');
  });
});
