//THIS CODE CONTROLS CURRENT LOCATION AND WEATHER UPDATES USING THE OPENWEATHER API
function getWeather() {
  var getIP = 'http://ip-api.com/json/';
  var openWeatherMap = 'http://api.openweathermap.org/data/2.5/weather';
  $.getJSON(getIP).done(function(location) {
      $.getJSON(openWeatherMap, {
          lat: location.lat,
          lon: location.lon,
          units: 'imperial',
          APPID: 'eef22c2ba9fcd342273350be7caaa690';
      }).done(function(weather) {
          document.getElementById('temp').innerHTML = Math.round(weather.main.temp);
          document.getElementById('location').innerHTML = location.city;
      })
  })
}


setInterval(function(){getWeather()}, 1000);
