<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>APP</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="author" content="Mujo Music">
    <link rel="shortcut icon" href="images/favicon.png">
    <link rel="apple-touch-icon" href="images/favicon.png">
    <script src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>
    <link href="https://fonts.googleapis.com/css?family=Barlow+Semi+Condensed" rel="stylesheet">
    <script src="https://maps.googleapis.com/maps/api/js?libraries=places&key=AIzaSyBDw8P280gnlBliOpGjYeH3OOPvP0P2WVE"></script>

    <link rel="stylesheet" href="style.css">
  </head>
  <body>

    <div class="app-wrapper">

        <img src="images/search-icon.png" alt="" id="search-icon">

        <form action="http://www.google.com/search" class="searchform" method="get" name="searchform" target="_blank">
        <input autocomplete="off" class="search" name="q" placeholder="Search..." required="required"  type="text">
        </form>

        <div class="location-wrapper">

          <img src="images/location.png" alt="" id="location-icon"><p id="location"></p><span id="slash">/</span><p id="temp"></p><img src="images/degrees.png" alt="" id="degree-symbol">

        </div>

      <p id="date"></p>

      <div class="overlay-container">

        <p id="time"></p>

        <p id="greeting"></p>

      </div>

      <div class="to-do-list-container">

        <img src="images/toggle-todo.png" alt="" id="toggle-todo">

        <div id="todo-div" class="header">
          <input type="text" id="myInput" placeholder="New To-Do..." onKeyDown="if(event.keyCode==13){newElement(); return false;}">
        </div>

      <ul id="todo-ul">

      </ul>

      </div>

    </div>

    <script src="DN.js" charset="utf-8"></script>
    <script src="DT.js" charset="utf-8"></script>
    <script src="TD.js" charset="utf-8"></script>
    <script src="GT.js" charset="utf-8"></script>
    <script src="WEATHER.js" charset="utf-8"></script>

  </body>
</html>
