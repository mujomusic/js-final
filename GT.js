//THIS SCRIPT CONTROLS THE DISPLAY OF THE CURRENT TIME AND A GREETING IN ACCORDANCE WITH THAT TIME

function checkTime(i) {
  if (i < 10) {
    i = "0" + i;
  }
  return i;
}

function startTime() {
  var today = new Date();
  var h = today.getHours();
  var m = today.getMinutes();
  var s = today.getSeconds();
  // add a zero in front of numbers<10
  m = checkTime(m);
  s = checkTime(s);
  document.getElementById('time').innerHTML = h + ":" + m;
  t = setTimeout(function() {
    startTime()
  }, 500);
}
startTime();

var today = new Date()
var curHr = today.getHours()

if (curHr > 0 && curHr < 12) {
  document.getElementById('greeting').innerHTML = "Good Morning, Mujo";
} else if (curHr > 12 && curHr < 18) {
  document.getElementById('greeting').innerHTML = "Good Afternoon, Mujo";
} else {
  document.getElementById('greeting').innerHTML = "Good Evening, Mujo";
}
